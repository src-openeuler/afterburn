%bcond_without check
%global __cargo_skip_build 0

%global dracutmodulesdir %(pkg-config --variable=dracutmodulesdir dracut || echo '/usr/lib/dracut/modules.d')

%global crate afterburn

Name:           rust-%{crate}
Version:        5.7.0
Release:        1
Summary:        Simple cloud provider agent

License:        Apache-2.0
URL:            https://crates.io/crates/afterburn
Source0:        %{crate}-%{version}.crate
Source1:        %{crate}-%{version}-vendor.tar.gz

BuildRequires:  rust-packaging openssl-devel glibc-devel
BuildRequires:  systemd

%global _description %{expand:
Simple cloud provider agent.}

%description %{_description}

%package     -n %{crate}
Summary:        %{summary}
License:        ASL 2.0 and MIT and BSD and 0BSD
%{?systemd_requires}

%description -n %{crate} %{_description}

%files       -n %{crate}
%license LICENSE
%doc README.md
%{_bindir}/afterburn
%{_unitdir}/afterburn.service
%{_unitdir}/afterburn-checkin.service
%{_unitdir}/afterburn-firstboot-checkin.service
%{_unitdir}/afterburn-sshkeys@.service
%{_unitdir}/afterburn-sshkeys.target

%post        -n %{crate}
%systemd_post afterburn.service
%systemd_post afterburn-checkin.service
%systemd_post afterburn-firstboot-checkin.service
%systemd_post afterburn-sshkeys@.service

%preun       -n %{crate}
%systemd_preun afterburn.service
%systemd_preun afterburn-checkin.service
%systemd_preun afterburn-firstboot-checkin.service
%systemd_preun afterburn-sshkeys@.service

%postun      -n %{crate}
%systemd_postun afterburn.service
%systemd_postun afterburn-checkin.service
%systemd_postun afterburn-firstboot-checkin.service
%systemd_postun afterburn-sshkeys@.service

%package     -n %{crate}-dracut
Summary:        Dracut modules for afterburn
BuildRequires:  pkgconfig(dracut)
Requires:       %{crate}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       dracut
Requires:       dracut-network

%description -n %{crate}-dracut
Dracut module that enables afterburn and corresponding services
to run in the initramfs on boot.

%files       -n %{crate}-dracut
%{dracutmodulesdir}/30afterburn/

%prep
%autosetup -n %{crate}-%{version} -p1

tar xvf %{SOURCE1}

mkdir -p .cargo
cat >.cargo/config << EOF

[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

# afterburn-sshkeys@.service is by default enabled for the 'core' user in NestOS
sed -e 's,@DEFAULT_INSTANCE@,core,' < \
  systemd/afterburn-sshkeys@.service.in > \
  systemd/afterburn-sshkeys@.service


%build
%cargo_build

%install
%cargo_install
install -Dpm0644 -t %{buildroot}%{_unitdir} \
  systemd/*.service systemd/*.target
mkdir -p %{buildroot}%{dracutmodulesdir}
cp -a dracut/* %{buildroot}%{dracutmodulesdir}

cp -r $RPM_BUILD_DIR/afterburn-5.7.0/.cargo/bin %{buildroot}/usr

%changelog
* Thu Nov 28 2024 dongjiao <dongjiao@kylinos.cn> - 5.7.0-1
- upgrade version to 5.7.0
  Add support for Proxmox VE

* Wed Aug 14 2024 yanjianqing<yanjianqing@kylinos.cn> - 5.6.0-1
- upgrade version to 5.6.0

* Fri Mar 15 2024 lilu <lilu@kylinos.cn> - 5.5.1-1
- upgrade version to 5.5.1

* Wed Jan 03 2024 duyiwei <duyiwei@kylinos.cn> - 5.4.3-1
- upgrade version to 5.4.3

* Mon Aug 07 2023 duyiwei <duyiwei@kylinos.cn> - 5.4.2-1
- upgrade version to 5.4.2

* Fri Aug 04 2023 duyiwei <duyiwei@kylinos.cn> - 5.4.1-1
- upgrade version to 5.4.1

* Fri Dec 9 2022 duyiwei <duyiwei@kylinos.cn> - 5.3.0-2
- Enable debuginfo for fix strip

* Tue Jul 19 2022 duyiwei <duyiwei@kylinos.cn> - 5.3.0-1
- upgrade version to 5.3.0

* Thu Jun 2 2022 duyiwei <duyiwei@kylinos.cn> - 5.2.0-1
- upgrade version to 5.2.0

* Fri Dec 24 2021 duyiwei <duyiwei@kylinos.cn> - 5.1.0-1
- Package init
