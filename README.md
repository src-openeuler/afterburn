# afterburn

#### 介绍
Afterburn is a one-shot agent for cloud-like platforms which interacts with provider-specific metadata endpoints. It is typically used in conjunction with Ignition.

#### Features
It comprises several modules which may run at different times during the lifecycle of an instance.

Depending on the specific platform, the following services may run in the initramfs on first boot:

-setting local hostname
-injecting network command-line arguments

The following features are conditionally available on some platforms as systemd service units:

-installing public SSH keys for local system users
-retrieving attributes from instance metadata
-checking in to the provider in order to report a successful boot or instance provisioning


#### 安装教程

Install afterburn rpm package:

yum install afterburn

#### 使用说明

afterburn 是云底座操作系统NestOS的必需组件

#### 参与贡献

master分支使用最新的上游版本，如果检测到上游有最新版本发布，先形成issue后再提交对应PR更新，流程如下。
1.  提交issue
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
